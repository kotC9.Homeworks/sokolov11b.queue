﻿using System;

namespace Queues.C
{
    public class GeneratorEventArgs : EventArgs
    {
        public GeneratorEventArgs(int value)
        {
            Value = value;
        }

        public int Value { get; }
    }
    
    public class BaseGenerator
    {
        public event EventHandler<GeneratorEventArgs> Generated;

        public virtual void Generate()
        {
            
        }
        
        protected void Invoke(int value)
        {
            var handler = Generated;
            handler?.Invoke(this, new GeneratorEventArgs(value));
        }
    }
}