﻿using System;
using System.Timers;

namespace Queues.C
{
    class Program
    {
        static void Main(string[] args)
        {
            var queueHandler = new QueueHandler(new ValueGenerator());
            queueHandler.Start();

            Console.ReadLine();
        }
    }
}