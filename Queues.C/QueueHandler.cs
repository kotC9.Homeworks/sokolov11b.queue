﻿using System;
using System.Collections.Generic;

namespace Queues.C
{
    /// <summary>
    /// processing values in the queue
    ///
    /// divide the queue into three parts
    /// (...max) (val val val val val) (...max)
    /// </summary>
    public class QueueHandler
    {
        private int _firstMax= int.MinValue;
        private int _secondMax= int.MinValue;
        private int _maxSum = int.MinValue;
        private Queue<int> _values = new Queue<int>();
        
        private readonly BaseGenerator _generator;

        public QueueHandler(BaseGenerator generator)
        {
            _generator = generator;
        }
        
        public void Start()
        {
            _generator.Generated += OnGeneratedValue;
            _generator.Generate();
        }

        private void OnGeneratedValue(object? sender, GeneratorEventArgs e)
        {
            var value = e.Value;
            _values.Enqueue(value);

            if (_values.Count > 5)
            {
                _firstMax = Math.Max(_firstMax, _values.Dequeue());
                _secondMax = Math.Max(_secondMax, value);
                _maxSum = Math.Max(_maxSum, _firstMax + _secondMax);
                Console.WriteLine($"бОльшая сумма: {_maxSum}");
            }
        }
    }
}