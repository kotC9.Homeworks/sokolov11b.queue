﻿using System;

namespace Queues.C
{
    public class TestGenerator : BaseGenerator
    {
        private Action _action;
        /// <summary>
        /// generate `n` values
        /// </summary>
        /// <param name="n">number of values</param>
        public TestGenerator(int n)
        {
            _action = () =>
            {
                var random = new Random();
                for (var i = 0; i < n; i++)
                {
                    Invoke(random.Next(-10000, 10000));
                }
            };
        }
        
        /// <summary>
        /// generate values of nums
        /// </summary>
        /// <param name="nums">array</param>
        public TestGenerator(params int[] nums)
        {
            _action = () =>
            {
                foreach (var num in nums)
                {
                    Invoke(num);
                }
            };
        }

        public override void Generate()
        {
            _action.Invoke();
        }
    }
}