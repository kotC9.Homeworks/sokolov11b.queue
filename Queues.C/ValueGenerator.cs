﻿using System;
using System.Timers;

namespace Queues.C
{
    public class ValueGenerator : BaseGenerator
    {
        private readonly Random _random;
        
        public ValueGenerator()
        {
            _random = new Random();
        }

        public override void Generate()
        {
            var timer = new Timer(1000);
            timer.Elapsed += OnTimedEvent;
            timer.AutoReset = true;
            timer.Enabled = true;
        }

        private void OnTimedEvent(object sender, ElapsedEventArgs e)
        {
            var value = _random.Next(-10000, 10000);
            Invoke(value);
        }
    }
}