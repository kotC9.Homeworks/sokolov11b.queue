Задание: Каждую секунду на вход программы поступает результат измерения – целое число. Данных настолько много, что хранить их все в памяти невозможно. Нужно написать программу, которая находит наибольшую сумму двух результатов измерений, между которыми прошло более 5 секунд.

Запуск с передачей класса ValueGenerator для получения рандомных значений каждые 1000мс.

Запуск с передачей класса TestGenerator для получения рандомных n значений, либо заданных вручную чисел
